<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/app/init.php');

class ApiController {

    protected $database;

    public function __construct($database) {
        $this->database = $database;
    }
    public function handleRequest() {
        $method = $_SERVER['REQUEST_METHOD'];
        $url = $_SERVER['REQUEST_URI'];
        $data = json_decode(file_get_contents('php://input'), true);
        $url = setURL($url);
        switch ($method) {
            case 'GET':
                // Handle GET request
                $id = '';
                $response = $this->handleGetRequest($url, $id);
                break;
            case 'POST':
                // Handle POST request
                $response = $this->handlePostRequest($url, $data);
                break;
            case 'PUT':
            case 'PATCH':
                // Handle PUT or PATCH request
                $response = $this->handlePutRequest($url, $data);
                break;
            case 'DELETE':
                // Handle DELETE request
                break;
            default:
                $response = "Invalid request method";
                break;
        }

        echo json_encode($response);
    }


    private function handleGetRequest($url, $id) {
        // if  content is empty
        $content = $this->database->table('sharing')->where('name', $url)->get('content');
        $data = array();
        if(empty($content)) {
            $data = [
                "content" => null
            ];
            return $data;
        }
        
        // if note is locked
        $is_note_locked = $this->database->table('sharing')->where('name', $url)->get('is_note_locked')[0]->is_note_locked;
        $data = [
            "note_locked" => $is_note_locked
        ];
        if($is_note_locked) {
            return $data;
        }


        // if note is not locked then return data
        $data = [
            "content" => $content[0]->content
        ];

        return $data;
        
        // $data = array();
        // if (empty($content)) {
        //     return '';
        // }

        // if ($is_note_locked) {
        //     $data["is_note_locked"] = $is_note_locked;
        // } else {
        //     $data["is_note_locked"] = $is_note_locked;
        //  w   $data["content"] = $content;
        // }

        // dd($data);
        return $content;
    }


    private function handlePostRequest($url, $data) {
        $result = $this->database->table('sharing')->insert(['name' => $url, 'content' => $data['content'], 'is_note_locked' => $data['is_note_locked']]);

        return $result;
    }

    private function handlePutRequest($url, $data) {
        $result = $this->database->table('sharing')->where('name', $url)->update($data);

        return $result;
    }
}
