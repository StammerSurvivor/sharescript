const textarea = document.querySelector('textarea');
const windowPath = document.querySelector('.window-path');
const messageToast = document.querySelector('.message-toast');
const overlay = document.querySelector('.overlay');
const noteLocked = document.querySelector('.note-locked');

/***************************************************************
************************ HELPER FUNCTIONS **********************
*****************************************************************/
function getURL(path) {
    const protocol = window.location.protocol;
    const host = window.location.host;
    const url = protocol + '//' + host + path;
    return url;
}


/***************************************************************
*********************** GET: CONTENT **************************
*************************************************************/

window.addEventListener('load', (e) => {
    textarea.innerHTML = '';
    let name = window.location.hash.substring(1, window.location.hash.length);
    const url = getURL(`/php/DatabaseController.php/%23${name}`);
    const request = new HttpRequest();
    request.setMethod('GET');
    request.setUrl(url);
    request.send(function (response) {
        console.log("🚀 ~ file: app.js:29 ~ response:", response);
        if (response.note_locked) {
            textarea.innerHTML = `
            Computer programming is the process of telling a computer to do certain things by giving it instructions. These instructions are called programs. A person who writes instructions is a computer programmer. The instructions come in different languages; they are called programming languages. There are many programming languages. Sometimes, programmers use special software, such as integrated development environments (IDEs), which have many special parts, including a text editor, to help them to type and edit programs.

            Computers understand instructions directly if those instructions are written in machine code, special characters that can be processed by the computer but are difficult for humans to read. Writing these instructions directly in machine code takes a long time and is difficult, so instructions are written in a language easier for people to use, which the computer converts into "computer form" instructions (in other words, machine code) so the computer can follow them. The simplest of these is assembly language. Programs written in a language more like English are converted by a compiler. Some languages, called interpreted languages, use interpreters instead of assemblers or compilers.
            
            Once a program has been compiled the instructions in it are written into a file that contains a series of numbers that the computer can understand. These machine-instructions will be loaded into the computer's memory so that the CPU can run them when the executable is opened. In the event that data is not transferred correctly, it can result in data loss.
            
            Variables
            A variable is a data piece in the program that can be changed. Variables can include information added by the user, or answers that are found as a result of a calculation.
            
            Conditionals
            Conditionals are parts of the program that work if something the program can check to see whether it is true. If that part is not true, then the program won't make it happen. A conditional is often done with an "If Statement".
            
            Here is an example of an if statement in the Perl programming language. What it does is it checks to see if the name variable is Bill. If the name variable is Bill, then it will print out the words "Hi Bill!".
            
            #!/usr/local/bin/perl
            $name = "Bill";
            
            if ($name eq "Bill") {
                print "Hi Bill!";
            }
            Sometimes, a programmer might want to have the if statement do something else if the first part of it is not true. This is known as an else block. Here is an example of an else block in the Perl programming language.
            
            #!/usr/local/bin/perl
            $name = "Ted";
            
            if ($name eq "Bill") {
                print "Hi Bill!";
            }
            else {
                print "Hi person who is not Bill!";
            }
            And sometimes, the programmer might want to have multiple things for the if statement to do. For example, they may have the if block run if something is true, but will have parts of the if statement known as else if blocks that will run if the first part doesn't work, but if it works somewhere else. In the Perl programming language, else if is spelled like this, "elsif." Other languages might have it spelled like "else if" however. But for Perl, it is spelled like "elsif." Else If blocks will only run if their condition is true, just like the first if block. An if statement can have as many else if statements as the programmer needs. If the if block, and none of the else if blocks are true, then the plain "else" statement will be used by the program.
            
            #!/usr/local/bin/perl
            $name = "Ted";
            
            if ($name eq "Bill") {
                print "Hi Bill!";
            }
            elsif ($name eq "Ted") {
                print "Hi Ted!";
            }
            elsif ($name eq "Alex"){
                print "Hi Alex!";
            }
            else {
                print "Hi other person!";
            }
            Comments
            In the program, a comment is information that is meant to be read by people who are reading the program. Comments have a special symbol in front of them that tells the computer that they are comments and should not be read as code.
            
            Comments are used to explain how a certain part of a program works. This is helpful when multiple people are working on the same program, and if they need to work on a section where someone else was working on. If the programmer that was working on it first left behind comments for any other programmer that works on it later, it will help them know faster what is going on in the program.
            
            Here is an example of programming comments in the C programming language. In C, the two slash symbols "//" known as a forward slashes, are used. With the comments, a person can read the code and know what is going on.
            
            #include <stdio.h>
            // This is a comment, ignored by the computer
            int main(void) { // Here the starting point of the program is defined
                printf("Hello world!\n"); // Actual process
                return EXIT_SUCCESS; // Tell everyone that we had success
            }
            Sometimes, a programmer may need to remove something from the code, but for many different reasons, they may not want to just simply delete it. An easy solution is to use the comment symbol. The computer will think that the code is just comments, rather than actual code, but the programmer will still be able to see it and read it.
            
            Here is an example of that in the Perl programming language. In Perl, the "#" symbol is used for comments, instead of forward slashes "//" like in the C (programming language).
            
            $name = "Sam"; #we set the name variable to be Sam
            $age = 14; #We set the age to be 14
            # $country = "France";  because of the special comment symbol at the beginning of the line, this line is now a comment.
            Debugging
            Computer programmers make mistakes when writing codes. The mistakes are called bugs and cause the program to follow the wrong instructions. Debugging is the process of finding and fixing the mistakes. To debug code is to find such mistakes.
            
            There are many debugging methods. Software such as text editors and IDEs have tools that can detect specific mistakes in the codes before the program is executed. Programmers can also use programs called debuggers. A debugger can run a program step by step and track how values of specific variables change when the program is running. Programmers can use a debugger to find where the mistake happened in the code.
            `;
            overlay.classList.add('active');
            noteLocked.classList.add('active');
        } else {
            textarea.innerHTML = '';
            overlay.classList.remove('active');
            noteLocked.classList.remove('active');
            if (!response.content) {
                createPageContent(name);
            } else {
                textarea.innerHTML = response.content;
                updatePageContent(name);
            }
        }
    });

})


window.addEventListener('unload', (e) => {
    e.preventDefault();
    let name = window.location.hash.substring(1, window.location.hash.length);
    const url = getURL(`/php/DatabaseController.php/%23${name}`);
    const request = new HttpRequest();
    request.setMethod('PUT');
    request.setUrl(url);
    request.setBody({
        is_note_locked: 0
    });

    request.send(function (response) {
        console.log(response);
    });

    return "gautam";
});


/************************************************************
********************* POST: CREATE CONTENT ******************
*************************************************************/
function createPageContent(name) {
    let typingTimer;
    let doneTypingInterval = 500;
    textarea.addEventListener('input', (e) => {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(() => {
            createPage(name, textarea.value)
        }, doneTypingInterval)
    });
}

function createPage(name, content) {
    const url = getURL(`/php/DatabaseController.php/%23${name}`);
    const request = new HttpRequest();
    request.setMethod('POST');
    request.setUrl(url);
    request.setBody({
        content: content,
        is_note_locked: 1,
    });
    request.send(function (response) {
        if (Boolean(response)) {
            messageToast.classList.add('active');
            setTimeout(() => {
                messageToast.classList.remove('active');
            }, 2000);
        }
    });
}

/************************************************************
*********************** PUT: UPDATE *************************
*************************************************************/
function updatePageContent(name) {
    let typingTimer;
    let doneTypingInterval = 500;
    textarea.addEventListener('input', (e) => {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(() => {
            console.log(textarea.value);
            updatePage(name, textarea.value)
        }, doneTypingInterval)
    });
}

function updatePage(name, content) {
    const url = getURL(`/php/DatabaseController.php/%23${name}`);
    const request = new HttpRequest();
    request.setMethod('PUT');
    request.setUrl(url);
    request.setBody({
        content: content,
        is_note_locked: 1,
    });

    request.send(function (response) {
        if (Boolean(response)) {
            messageToast.classList.add('active');
            setTimeout(() => {
                messageToast.classList.remove('active');
            }, 2000);
        }
    });
}