<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/app/init.php');

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shrib</title>
    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="./css/reset.css">
    <link rel="stylesheet" href="./css/main.css">

    <!-- GOGGLE FONTS -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Space+Grotesk:wght@300;400;500;600;700&display=swap" rel="stylesheet">
</head>

<body>
    <div class="container">
        <textarea></textarea>
        <div class="message-toast">
            <span><ion-icon name="checkbox-outline"></ion-icon></span>
            <p>Context Saved</p>
        </div>
    </div>


    <!-- OVERLAY -->
    <div class="overlay"></div>


    <!-- NOTE LOCKED -->
    <div class="note-locked">
        <p><ion-icon name="lock-closed-outline"></ion-icon></p>
        <h1>Note Locked</h1>
    </div>


    <!-- CUSTOM JS -->
    <script src="./js/HttpRequest.js"></script>
    <script src="./js/app.js"></script>
    <!-- ICONS -->
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
</body>

</html>