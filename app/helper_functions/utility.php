<?php

function dd($data) {
    echo "<pre style='font-size:1.5rem'>";
    die(var_dump($data));
}

function redirect($url) {
    return header("Location: $url");
}

function getCurrentTimeInMillis() {
    return round(microtime(true) * 1000);
}
function abort($statusCode = 200) {
    http_response_code($statusCode);
    exit(403);
}


function generateUniqueId() {
    // Generate a random binary string
    $binaryString = random_bytes(16);

    // Convert the binary string to a hexadecimal string
    $hexString = bin2hex($binaryString);

    // Output the hexadecimal ID
    return $hexString;
}


function getURL($uri) {
    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";
    $host = $_SERVER['HTTP_HOST'];
    $url = $protocol . "://" . $host . $uri;
    return $url;
}


function setURL($url) {
    $url = basename($url);
    $url = str_replace('%23', '#', $url);
    $url = str_replace("/", "", $url);
    return $url;
}
