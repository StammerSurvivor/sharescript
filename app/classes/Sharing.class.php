<?php 
  
  class Sharing {
    static public Database $database;
    static protected string $table = "sharing";
    protected $attb = array();
    public function __set($name, $value) { $this->attb[$name] = $value; }
    public function __get($name) { return $this->attb[$name] ?? null; }
    public static function build() {
      $sql = "CREATE TABLE IF NOT EXISTS " . static::$table . "(
              id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
              name VARCHAR(20) NOT NULL UNIQUE,
              content VARCHAR(100000) NOT NULL UNIQUE,
              is_note_locked BOOLEAN DEFAULT FALSE,
              created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
              updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
          )";
          
      self::$database->query($sql);
    }
  }

?>