<?php
class Database {

    protected PDO $pdo;
    protected $dataBag = array();
    protected string $table;
    protected string $where;
    // static public 

    public function __construct($host = 'localhost', $db = 'shrib', $port = 3306, $username = 'gautam', $password = 'gautamchimnani', $fetchMode = PDO::FETCH_OBJ) {
        $this->pdo = new PDO("mysql:host={$host};port={$port};dbname={$db}", $username, $password);
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, $fetchMode);
        $this->resetFields();
    }

    // Used to hit DDL statements
    public function query($sql) {
        return $this->pdo->query($sql);
    }

    /**
     * use this to fire raw Select queries
     */

    public function rawQueryExecutor($sql) {
        try {
            return $this->query($sql)->fetchAll();
        } catch (PDOException $e) {
            echo "Error executing query: " . $e->getMessage();
            // or log the error to a file or database
            return false;
        }
    }

    public function table($table) {
        $this->table = $table;

        return $this;
    }

    public function where(string $field, string $value, string $operator = "=") {
        $this->dataBag["WHERE$field"] = $value;

        if ($this->where === "1") {
            $this->where = "$field $operator :WHERE$field";
        } else {
            $this->where = $this->where . " AND $field $operator :WHERE$field";
        }
        return $this;
    }

    public function get($columns = "*") {
        $sql = $this->prepareSQLQuery($columns);

        $ps = $this->pdo->prepare($sql);

        $ps->execute($this->dataBag);
        $this->resetFields();
        return $ps->fetchAll();
    }

    public function first($columns = "*") {

        $sql = $this->prepareSQLQuery($columns);
        $sql = $sql . " LIMIT 0, 1";
        $ps = $this->pdo->prepare($sql);
        $ps->execute($this->dataBag);
        // it reset to initial state and old values get removed
        $this->resetFields();
        $data = $ps->fetchAll();
        return !empty($data) ? $data[0] : null;
    }

    public function count() {
        $as = "count";
        $sql = "SELECT count(*) as $as FROM {$this->table} WHERE {$this->where}";
        $ps = $this->pdo->prepare($sql);
        $ps->execute($this->dataBag);
        $this->resetFields();
        return $ps->fetchAll()[0]->$as;
    }

    public function insert($data) {
        $keys = array_keys($data);

        $fields = "`" . implode("`, `", $keys) . "`";
        $placeholder = ":" . implode(", :", $keys);

        $sql = "INSERT INTO {$this->table} ($fields) VALUES ($placeholder)";
        $ps = $this->pdo->prepare($sql);
        return $ps->execute($data);
    }

    public function update(array $data) {
        $updationString = "";
        foreach ($data as $key => $value) {
            $updationString = $updationString . " $key = :$key,";
        }
        $updationString = rtrim($updationString, ",");
        $sql = "UPDATE {$this->table} SET {$updationString} WHERE {$this->where}";
        $dataWithConditionalParams = array_merge($data, $this->dataBag);
        $ps = $this->pdo->prepare($sql);
        $this->resetFields();
        return $ps->execute($dataWithConditionalParams);
    }

    public function delete() {
        $sql = "DELETE FROM {$this->table} WHERE {$this->where}";
        $dataParams = $this->dataBag;
        $ps = $this->pdo->prepare($sql);
        $this->resetFields();
        return $ps->execute($dataParams);
    }

    public function exists(mixed $data) {
        foreach ($data as $key => $value) {
            $this->where($key, $value);
        }

        return $this->count() >= 1 ? true : false;
    }

    private function resetFields() {
        $this->table = "";
        $this->where = "1";
        $this->dataBag = array();
    }

    private function prepareSQLQuery($columns) {
        return "SELECT $columns FROM {$this->table} WHERE {$this->where}";
    }

    public function isTableExist() {
        $sql = "SHOW TABLES LIKE '%$this->table%'";
        $stmt = $this->query($sql);
        $stmt->execute();
        return $stmt->rowCount() > 0;
    }
}
